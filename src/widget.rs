use eframe::egui::{Ui, Widget, Response};
use markdown::{mdast::Node, to_mdast, ParseOptions};

pub struct MarkdownWidget(Node);

impl MarkdownWidget {
    fn render(&self, ui: &mut Ui){
        for c in self.0.children().unwrap() {
            match &c {
                Node::Root(r) => for rc in &r.children{
                    ui.add(Self(rc.clone()));
                },
                Node::Paragraph(_) => {
                    ui.label(get_text(c));
                }
                Node::Heading(_) => {
                    ui.heading(get_text(c));
                }
                Node::Link(a) => {
                    println!("{}, {}", a.title.as_ref().unwrap(), a.url);
                    if a.title.is_some() {
                        ui.hyperlink_to(a.title.as_ref().unwrap(), &a.url);
                    } else {
                        ui.hyperlink(&a.url);
                    }
                }
                _ => (),
            }
        }
    }
    
    pub fn new(node: String) -> Self {
        let ast = to_mdast(&node, &ParseOptions::default());
        if let Ok(c) = ast{
          return Self(c);
        }
        Self(ast.unwrap())
    }
}

impl Widget for MarkdownWidget {
    fn ui(self, ui: &mut Ui) -> Response {
        ui.vertical(|ui| self.render(ui)).response
    }

}


fn get_text(md: &Node) -> &str {
    if let Some(c) = md.children().unwrap().iter().next() {
        return match c {
            Node::Text(a) => &a.value,
            _ => "",
        };
    }
    ""
}
